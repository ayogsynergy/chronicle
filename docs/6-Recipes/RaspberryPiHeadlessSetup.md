# Raspberry Pi Headless Setup

Need a second computer to setup the image.

# Download the image
[RaspberryPi Image OS](https://www.raspberrypi.org/downloads/raspberry-pi-os/)  
Download `Raspberry Pi OS (32-bit) with desktop` image  

# Copy the OS image to SD Card
Can use rufus or etcher. 

# Customize Raspian
Look for `boot` partition on the sd card. It will be readable by Windows by default. It is < 0.5 GB in size, for reference. 

## Enable WIFI
Define a `wpa_supplicant.conf` file for the particular wifi to connect to.
```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=<Insert 2 letter ISO 3166-1 country code here>

network={
 ssid="<Name of your wireless LAN>"
 psk="<Password for your wireless LAN>"
 scan_ssid=1
}
```
A filled out example
```
country=US
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
 ssid="mytheoreticalwifiname"
 psk="mytheoreticalpasswordtowifi"
 scan_ssid=1
}
```  

Save this file as `wpa_supplicant.conf` in the root of the `boot` partition. Do this prior to booting your PI.

## Enable Remote access
Activite ssh by adding an empty `ssh` file to the `boot` partition. No extensions. Case sensitive.

# Verify
Insert the sd card on the PI and turn it on. Ensure that your PI supports WIFI or has an associated WIFI dongle. Next, find a way to connect to it.

```bash
nmap -sn 192.168.102.0/24
...
...
Nmap scan report for 192.168.102.158
Host is up (0.12s latency).
MAC Address: B8:27:EB:8E:88:C0 (Raspberry Pi Foundation)
```
Check to see that the `ssh` file was located on the boot parition, indicating to the raspberry pi to enable the SSH daemon.
```
nmap 192.168.102.158 -T4 -F
Starting Nmap 7.80 ( https://nmap.org ) at 2020-11-08 21:30 Eastern Standard Time
Nmap scan report for 192.168.102.158
Host is up (0.011s latency).
Not shown: 99 closed ports
PORT   STATE SERVICE
22/tcp open  ssh
MAC Address: B8:27:EB:8E:88:C0 (Raspberry Pi Foundation)
```

# How to connect to raspberry pi
There are several ways to connect to the raspberry pi.

`raspberrypi` is the default hostname entry on startup. Remember to update this to something else    in `/etc/hostname`

```
ping raspberrypi

Pinging raspberrypi.local [fe80::3979:e599:b381:1ea6%17] with 32 bytes of data:
Reply from fe80::3979:e599:b381:1ea6%17: time=23ms
Reply from fe80::3979:e599:b381:1ea6%17: time=2ms
Reply from fe80::3979:e599:b381:1ea6%17: time=1ms
Reply from fe80::3979:e599:b381:1ea6%17: time=2ms

Ping statistics for fe80::3979:e599:b381:1ea6%17:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 1ms, Maximum = 23ms, Average = 7ms
```

Find the default credentials for raspberry pi can be found [here](https://www.raspberrypi.org/documentation/linux/usage/users.md). Update the credentials immediately.