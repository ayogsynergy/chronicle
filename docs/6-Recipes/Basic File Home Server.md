# Basic File Home Server

# Objective
Many times I've tinkered with linux servers and 

# Goals
- Centralized file repository for files
- Ability to backup files and ensure redudancy
- Power savings


# OS of Choice - Ubuntu Server LTS 20.04
It is a safe choice to go with an LTS release as it will provide Long Term Support. An LTS version will have about 5 years of support so you can be sure you're version will be stable and will not be left behind. 

Ubunut Server is one of the more popular distributions of linux and has a lot of community support as well. This is not a hard requirement, but this guide will cannot guarantee this recipe to be 100% compatible.

# ZFS on Root
This allows you to run with ZFS on root, via the ZSys helper. This is currently only offered via the desktop version and is experimental. Proceed at your own discretion. I have opted out of this as I wished for a more stable environment. 