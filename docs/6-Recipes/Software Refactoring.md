# Software Refactoring

## Definition
`Refactoring` is the process of changing existing software to be forward thinking and streamlined for future growth. This should be a day-to-day process that every developer should include in their development life cycle.  

> Always leave things better than what you found them.  

Many assumptions are made when developing software, they can be correct or other times these assumptions are stale or absolutely wrong. It happens; however, if left unchecked, can lead to bigger hacky changes if developers aim to simply "add a feature or fix a bug".  

In addition, hindsight is 20/20 and once a design becomes clear, it's gaps or room for improvements become clear.  
Refactoring is a continual process to maintain the integrity of the architecture to stand the test of time.

## When to refactor
A quick and small program is always warranted, for POC or hotfixes. If it compiles, it can ship. If this program is a small piece of a bigger system, more priority to refactor to a better pattern is warranted.

## Why we want to refactor
Let's assume there is a deeply complex method that is tailor made for a certain view medium. If there is a request to have it html compliant, we are unable to resuse a lot of the logic from the original statement function. A solution is to copy paste the logic and make appropriate changes.

### Copy Paste 
What happens when the logic has to change and the original author is no longer working on this code? The new engineer might miss the fact that logic is duplicated and it requires _that_ much more brain power to catch. It could also be missed leading to regressions. As such, maintaining this type of code (code smell) is cumbersome and bug prone. 

### Tip
A novice developer might be tempted to make the fewest possible changes to the program. If the code is no longer aligned with the assumptions of the business (for future growth), as presented by a new feature (and potentially more to come); first, refactor the program to make it easy to add the feature then proceed to code the feature.

## Refactoring Playbook 

### Develop a test suite
A test suite will "lock" in the behavior of existing code. Ensure that a test suite exists for the code you wish to change, or make a new one. A solid set of tests with good coverage is key; it is not meant to be busy work, but more of a sanity check for our human tendencies to make mistakes.

This is out of the scope of this document, but read into effective test development for optimal code coverage. Consider red-green development.

By having an automated test suite, we save ourselves from hand checking outputs for verification.

## Decomposing and Redistributing a long winded statment
We want to decompose long spaning logical ideas into smaller manageable groups.  

A rough guideline is to dilineate where a scope of work can be extracted. Determine the inputs and outputs of this unit of work (if required). Run your tests and verify no regressions.  

The `Extract Method` found in modern IDEs is useful to do most of this boiler plate work for you. It will pick variable names for you where possible. We want to communicate the true intent of the code, so we update variables and entities were applicable. 

> Code that communicates the true intent of code is valueable.  

## Removing Temps
The longer a temporary variable exists, the less value it will bring to the current execution. They encourage long routines, which we want to get away from.

## Replacing Conditional Logic with Polymorphism
It is not a good idea to introduce a switch based on the attribute of another object for processing.