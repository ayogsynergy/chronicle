# Terminal kung foo

As a developer, a lot of my time is spent in the command line. Many of the tasks I do are the same and far out power the productivity a GUI can provide; however, even interacting with the command line can introduce overhead and be cumbersome.

As an aficionado of optimizing time efficiency, I love to push the limit to which I can automate and streamline the common repetitive tasks in the command.  

# Cmder
Introducing [https://cmder.net/](https://cmder.net/)
> Cmder is a software package created out of pure frustration over the absence of nice console emulators on Windows. It is based on amazing software, and spiced up with the Monokai color scheme and a custom prompt layout, looking sexy from the start.  

Cmder is my go-to tool for all things command line related. It is open sourced, it offers plenty of extensibility and portability.  

# Integration with IDEs
Cmder can be bootstrapped with a couple of parameters in the startup script of your terminal window. Then, you're free to focus all of your development tasks in one window.  

# Integration with VS Code (windows)
```json
{
    
    "terminal.integrated.shell.windows": "C:\\Windows\System32\cmd.exe",
    "terminal.integrated.env.windows": {
        "CMDER_ROOT": "C:\\compile\cmder_mini"
    },
    "terminal.integrated.shellArgs.windows": [
        "/k",
        "%CMDER_ROOT%\vendor\bin\vscode_init.cmd"
    ]
}
```
# Mintty
> Unix/Linux like bash shell running on Windows. See below for Mintty configuration differences  

> As a result mintty specific config is done via the [%USERPROFILE%|$HOME]/.minttyrc file.

Go to settings (right click on the title bar) `Win + Alt + P`  
Select `bash::mintty as Admin`  
Save

## user_profile.sh
```bash
alias gg="git status"
alias gl="git log --oneline --all --graph --decorate"
alias gd="git diff"
alias gcm="git checkout master"
alias gp="git pull"
alias br="git branch"
alias brr="git branch -r"
alias gca="git add . && git commit"
alias ar="source $CMDER_ROOT/config/user_profile.sh"
alias config="vim $CMDER_ROOT/config/user_profile.sh"
alias dev="cd '$DEV'"
alias e.="explorer ."
alias cmderr="cd '$CMDER_ROOT'"
```