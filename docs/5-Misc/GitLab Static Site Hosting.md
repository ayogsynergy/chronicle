# Static Site Hosting
Project pages owned by a user:  
http(s)://username.example.io/projectname

e.g. https://username.gitlab.io/projectname

Gitlab yml configuration:
```yml
image: node:latest

before_script:
  - npm ci

pages:
  stage: deploy
  script: 
    - cp -r docs/. public
  artifacts:
    paths:
      - public
  only:
    - master
```