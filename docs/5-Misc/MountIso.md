 # Windows Mount ISOs

 # Background
 I have backed up digital copies of my La Noire Disks I purchased some time back. I needed a Windows native way to mount these ISOs without having to download yet another tool.

 # Powershell
 [Reference](https://docs.microsoft.com/en-us/powershell/module/storage/mount-diskimage?view=win10-ps)

 ```powershell
 Mount-DiskImage
     [-ImagePath] <String[]>
     [-StorageType <StorageType>]
     [-Access <Access>]
     [-NoDriveLetter]
     [-CimSession <CimSession[]>]
     [-ThrottleLimit <Int32>]
     [-AsJob]
     [-PassThru]
     [-WhatIf]
     [-Confirm]
     [<CommonParameters>]
```

An example invocation  
Mount-DiskImage -ImagePath "C:\Users\RicoDell\Documents\gGames\LaNoire\LaNoireDisk1.iso"  

Windows will create a new volume path for this ISOs to which it can be invoked as normally done in a physical equivalent of the disk being loaded.