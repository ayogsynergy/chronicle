# Detecting Code Smells

Code smells are intrinsic factors of software that can, if unchecked, can increase the amount of cognitive complexity to understand.

# Cryptic Variable Names
If you're writing you're own codebase, you know what you mean when you write:
```javascript
const m = "Hi, this is my variable for which I know exactly what it means and it's use."
const h = "It's easy for me.";
const t = m.substring(3) + h;
```

But to the innocent reader, they have to stop and really break down what this code snippet is doing. For example, what is `m`? What is `h`? Can I infer what `t` means? Does it mean `text`? `total`?

This is just one example of a code smell. The intents of variables are not clear.  
One alternative might be:
```javascript
// main string
const m = "Hi, this is my variable for which I know exactly what it means and it's use."
// helper string
const h = "It's easy for me.";
// final text string
const t = m.substring(3) + n;
```

You can add comments; however, comments are not really necessary here. You can leverage the variable names to convey it's intent.
```javascript
const title = "Hi, this is my variable for which I know exactly what it means and it's use."
const subtitle = "It's easy for me.";
const concatenatedTitle = m.substring(3) + n;
```

While this is a extremely trivial example, you can see the value of conveying the intents in the variable names. It communicates it's use in the context. You also save yourself from having to comment your code. 

# Long functions
Let's think about Linux utilities. 
`man`, `ls`, `ip addr`, `nmap`, `pwd`, etc...  
These utilities do _one thing and one thing well_. It does not try to do too many things (albeit some utilities can do some fancy things; however within its own domain of utility).

Why not follow the same principle for functions? Functions should be small and concise. They should stick to doing one thing and one thing very well. It should not try to do too many things.

A function that does too many things will be prone to bugs and it becomes harder to isolate cross cutting concerns. 

There is not hard or fast rule but a good measure to follow is to ask if you can write a suite of unit tests that limits the amount of side effects and is limited in scope. This means not too many levels of nesting or too many decision points within the function.

# Duplicate code
This one is key. Keeping anything in sync requires effort. Anything that is duplicated is prone to diverge at some point.
> Developer A modifies `function123` in codebaseA but forgets to update `function123` in codebaseB  

Think twice about having to keep code in sync. It is a manual effort and is only a matter of time before someone forgets to update all instances of a block of code. It is expensive and time consuming.

There are different levels to this. If you are sharing code across code bases, consider leveraging a package manager to distribute and version your reusable code. If it's within the same code base, try to make utilities or a library namespace for your reusable code.

By doing this, you limit your refactoring surface when the time arrives to update your code.

# Too many parameters
Limit how many parameters you pass into any function. The longer a function signature becomes, the trickier it is to track the order for that function.

A solution is to encapsulate some parameters in an object that makes sense.

# Side Effects Everywhere!
Try to avoid side effects such as mutating variables behind the scenes. This can lead to unexpected behavior. It becomes hard to track data mutations if you heavily rely on data mutations as side effects inside of a code block. 

Approaching software in a functional way, where a routine/function to return an immutable object (with the changes you're interested in) can resolve a lot of bugs and makes writing tests easier.