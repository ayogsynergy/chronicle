# RGB LED

# Prequisites
- Arduino board
- Breadboard
- 1x  RGB LED
- 3x 220 ohm resistor
- 1k ohm resistor
- 4 x M-M wires

# Component
![RGB LED](/assets/electronics/lab4_rgbled.png)  
This LED has four leads, one for each LED and a common ground.

The cathode (-) is the second longest lead from the flat side of the LED.
220 Ohm resistor is required to prevent too much current from burning out the LED.

PWM output will produce a pulse depending on the amount spent in HIGH vs the LOW time in a period.

In the arduino, the period is set to about 1/500 of a second. Writing an 8 bit value will determine how much time during this period spent HIGH and the remaining LOW.

For example, an `analogWrite(255)` will produce a HIGH signal for the duration of the period; however, an `analogWrite(8)` the amount of power delivered in HIGH will be fractionally less.  

The result of analog PWN is that the brightness is adjusted since only a fraction of 5V is delivered for each period.

## Schematic
![schematic](/assets/electronics/lab4_schematic.png)

## Physical Representation
![Live Respresentation](/assets/electronics/lab4_live.jpg)

# Code
Psuedocode  
FOR loops will be used to cycle through the colors. 
We will initialize RED to be on (255), GREEN will be (0), and BLUE (0) 
The first for loop will decrease RED and increase GREEN.  
The second for loop will decrease GREEN and increase BLUE.
The final for loop will decrease BLUE and increase RED.

Source can be found [here](https://gitlab.com/ayogsynergy/embedded-systems-demo/-/blob/master/Labs/RGB_LED/RGB_LED.ino).