# Lesson 3 LED Brightness

# Prequisites
- Arduino board
- 5 mm LED
- 220 ohm resistor
- 1k ohm resistor
- 10k ohm resistor
- 2 x M-M wires
- Breadboard

# Resistor diagram
![Resistor Diagram](/assets/electronics/resistor_diagram.png)  

# Schematic
![Schematic Layout](/assets/electronics/lab3_schematic.png)  

 By replacing the different resistance ohm resistors, the brightness level will change. The UNO is providing a steady potential of 5V and by manipulating the resitance in the circuit the LED will emit different levels of brightness. The 220 ohm resistor will have the greatest brightness. The 10k ohm resistor will have the most dim LED.