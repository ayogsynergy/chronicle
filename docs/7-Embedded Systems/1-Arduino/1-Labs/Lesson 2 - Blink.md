# Blink on board LED  
The UNO R3 controller has a LED on board. Let's interface with it.  

It is referred to as the `L` LED, as seen on the board.

TODO: add a picture of the motherboard.  

# Default blink
By default the blink sketch is pre-installed when you plug in the device for the first time.  

# Load example sketch
```c
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(0500);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(0500);                       // wait for a second
}
```