# Serverless tutorial
[Tutorial Site](https://serverless-stack.com/chapters/what-is-aws-lambda.html)

Each function runs inside a container with a 64-bit Amazon Linux AMI. And the execution environment has:

- Memory: 128MB - 3008MB, in 64 MB increments
- Ephemeral disk space: 512MB
- Max execution duration: 900 seconds
- Compressed package size: 50MB
- Uncompressed package size: 250MB

CPU is not part of specification. Increasing the memory will increase the CPU specs too.  

Epehemeral disk space available on `/tmp` for temp purposes only.  
Lambdas are stateless. They can run for a max of 900 seconds (15 minutes)  

## Package size 
refers to all dependencies to run the function, and code.  

```javascript
exports.nameOfLambda = function (event, context, callback) {
    // do stuff
    callback(Error error, Object result);
}
```

event: contains info about event being triggered in the lambda. HTTP request will have info about the request.  
context: contains info about the runtime the lambda is executing in.  
when done, the callback function is invoked with the results (or error) and AWS will respond to the HTTP request with it.  

Lambda functions are packaged and sent to AWS. This is done via S3.  

## Stateless Functions
Every time function is invoked it is in a completely new environment. You don't have access to the execution context of the previous event.  

```javascript
var dbConnection = createNewDbConnection();

exports.myHandler = function(event, context, callback) {
  var result = dbConnection.makeQuery();
  callback(null, result);
};
```

The DB is instantiated once per container and not everytime the Lambda function is invoked. This is a caching effect.  
This is true of the `/tmp` directory also.  

## Pricing
Lambda functions are billed only for the time it takes to execute the function. Calculated from time it begins executing until when it returns or terminates. Rounded up to the nearest 100ms.  

Lambda comes with a generous free tier. Includes 1M free requests per month and 400,000 GB-seconds of compute time per month.  Past this, it costs $0.20 per 1 million requests and $0.00001667  

## Why serverless?
1. Low maintenance
2. Low cost
3. Easy to scale

## AWS Account
### Create an IAM user
Done.

### What is an IAM
AWS Identity and Access Management is a web service to help securely control access to AWS resources for users. You use IAM to control who can use AWS resources (authentitcation) and what resources they can use in what ways (authorization).  

# IAM User
Root user is assigned to creator of the AWS account.  
For one single developer, this is fine. Otherwise you can create an IAM user to allow for additional people on the same AWS account and specify IAM rules.  

# IAM Policy
A rule or set of rules defining the operations allowed/denied to be performed on an AWS resource.

can be granted by:
- attaching a managed policy. AWS provides a list of pre-defined policies such as AmazonS3ReadOnlyAccess
- attaching an inline policy. It's a policy created by hand.
- adding the user to a group that has the appropriate policies attached.
- cloning the permission of an existing IAM user.

# IAM Group
collection of IAM users. use groups to specify permissions for a collection of these users which can make those permissions easier to manage.  
e.g. a group called Admins and give that group the types of permissions that administrators typically need. 

# ARN
Amazon Resource Names. uniquely identify AWS resources across all of AWS. This included IAM policies, tags, API calls, etc.  
They can be used for communication, IAM policy

# Next steps
Created a DynamoDB table  
Created S3 bucket for file uploads  
Created user pool for authentication  
Pool Id us-east-1_gOy1vCaPk
Pool ARN `arn:aws:cognito-idp:us-east-1:622593731378:userpool/us-east-1_gOy1vCaPk`

Apps Client Id: 9j5ejk7gc7k5vat1c327goduj

# Follow up
https://serverless-stack.com/chapters/setup-the-serverless-framework.html  
https://serverless-stack.com/chapters/add-support-for-es6-es7-javascript.html  
Made a git project and pulled down serverless. Continue from here.