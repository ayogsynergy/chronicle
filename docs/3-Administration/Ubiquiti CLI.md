# Ubiquiti CLI Primer

```
ubnt@ubnt:~$ ?                                                                   
add            delete         ping6          reset          terminal            
clear          disconnect     reboot         restart        traceroute          
configure      generate       release        set            traceroute6         
connect        initial-setup  remove         show           undebug             
copy           no             rename         shutdown                           
debug          ping           renew          telnet     
ubnt@ubnt:~$ show ?                                                           
arp              flow-accounting  nat              table                        
bridge           hardware         ntp              tech-support                 
configuration    history          openvpn          ubnt                         
date             host             pppoe-server     users                        
debugging        incoming         queueing         version                      
dhcp             interfaces       reboot           vpn                          
dhcpv6           ip               route-map        vrrp                         
disk             ipv6             shutdown         webproxy                     
dns              lldp             snmp             zebra                        
file             log              switch                                        
firewall         login            system  
ubnt@ubnt:~$ show interfaces                                                    
Codes: S - State, L - Link, u - Up, D - Down, A - Admin Down                    
Interface    IP Address                        S/L  Description                 
---------    ----------                        ---  -----------                 
eth0         -                                 u/u                              
eth1         -                                 u/D                              
eth2         -                                 u/D                              
eth3         -                                 u/D                              
eth4         -                                 u/D                              
eth5         -                                 u/D                              
eth6         -                                 u/D                              
lo           127.0.0.1/8                       u/u  
```

To configure an IP address on eth0:
```
ubnt@ubnt# set interfaces ethernet eth0 address ?                                
Possible completions:                                                                
IP address and prefix length                                           
IPv6 address and prefix length                                    
dhcp          Dynamic Host Configuration Protocol                               
dhcpv6        Dynamic Host Configuration Protocol for IPv6   
ubnt@ubnt# set interfaces ethernet eth0 address 10.1.1.80/23               
ubnt@ubnt# set interfaces ethernet eth0 description "production LAN"    
```
These changes are only changes to the "working" configuration not the "active". To see what changes have been made in the "working" configuration, use the "compare" command:
```
ubnt@ubnt# compare                                                                              
+address 10.1.1.2/24                                                     
+description "production LAN"          
```
Then to make the changes active we use the "commit" command:

`ubnt@ubnt# commit                                                               `  
Assuming the commit didn't fail, then the changes are active. However if we reboot now those changes will be lost until we use the "save" command to store the "active" configuration to the boot configuration.

```
ubnt@ubnt# save                                                                 
Saving configuration to '/config/config.boot'...                                
Done                                                                              
ubnt@ubnt# exit                                                                 
exit                                                                            
ubnt@ubnt:~$ ubnt@ubnt:~$ show interfaces                                                    
Codes: S - State, L - Link, u - Up, D - Down, A - Admin Down                    
Interface    IP Address                        S/L  Description                 
---------    ----------                        ---  -----------                 
eth0         10.1.1.80/23                      u/u  production LAN              
eth1         -                                 u/D                              
eth2         -                                 u/D                              
eth3         -                                 u/D                              
eth4         -                                 u/D                              
eth5         -                                 u/D                              
eth6         -                                 u/D                              
lo           127.0.0.1/8                       u/u                                           
::1/128                                                                         
ubnt@ubnt:~$ ping 10.1.0.1                                                      
PING 10.1.0.1 (10.1.0.1) 56(84) bytes of data.                                  
64 bytes from 10.1.0.1: icmp_req=1 ttl=64 time=0.460 ms                         
64 bytes from 10.1.0.1: icmp_req=2 ttl=64 time=0.407 ms                         
^C                                                                              
--- 10.1.0.1 ping statistics ---                                                
2 packets transmitted, 2 received, 0% packet loss, time 999ms                   
rtt min/avg/max/mdev = 0.407/0.433/0.460/0.033 ms        
```