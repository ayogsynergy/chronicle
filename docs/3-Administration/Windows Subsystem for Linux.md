# Windows Subsystem for Linux (WSL)
Linux is the swiss army knife of most IT administration tasks; however, to the common user, the Windows operating system is a safe option. 

# Turn on the WSL Windows Feature  

1. Go to settings  
![settings](assets/wsl/01.png)

2. Enable  `Windows Subsystem for Linux`  
![enable](assets/wsl/02.png)

3. Restart your system.

# Install Distribution of choice  
Ubuntu is an example of different distributions for install.
![ubuntu install](assets/wsl/03.png)  

# Boot your distribution
![boot dist](assets/wsl/04.png)

The first time run with run the install sequence.  
![first time](assets/wsl/05.png)  

You will see the following prompt and you're ready to go.  
![prompt](assets/wsl/06.png)

# Access your local windows files
Local windows files can be found under `/mnt`

```bash
rico@DESKTOP-7J995EQ:/mnt/c/Users/RicoDell$ ll /mnt
total 0
drwxr-xr-x 1 root root 512 Nov  1 13:23 ./
drwxr-xr-x 1 root root 512 Nov  1 13:23 ../
drwxrwxrwx 1 root root 512 Nov  1 13:29 c/
rico@DESKTOP-7J995EQ:/mnt/c/Users/RicoDell$
```

# WSL FAQ
https://docs.microsoft.com/en-us/windows/wsl/faq#what-is-windows-subsystem-for-linux-wsl

## Git Credential Core Setup
You can allow windows to handle credentials for you. This will save you time if you enter in your credentials into your VCS of choice.  

Ensure that you install git onto your machine.
```bash
git config --global credential.helper "/mnt/c/Program\ Files/Git/mingw64/libexec/git-core/git-credential-manager-core.exe"
```

## Alias
Bash has support for alias. There is a bootstrap file, `.bashrc` usually located in the home directory.

```bash
...
# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi
...
```

`.bash_aliases` is sourced for all aliases you may want to define. 

Here is an example of the this file:
```bash
alias gg='git status'
alias dev="cd ${DEV}"
```  

Notice that `DEV` is a variable that is evaluated when the file is sourced.

# Upgrading from Version 1
If you are an early adopter you may want to upgrade to a newer version. Let's walk through up grading to version 2.
- Make sure the HyperV feature is on
- Follow this Microsoft [guide](https://docs.microsoft.com/en-us/windows/wsl/install-manual)

`wsl --list -v`  
Output
```
NAME                STATE       VERSION
* Ubuntu-20.04      Running     1
```

If version is not 2, set it to 2
`wsl --set-version Ubuntu-20.04 2`

Check once more after all operations have completed.
`wsl --list -v`  
Output
```
NAME                STATE       VERSION
* Ubuntu-20.04      Running     2
```

I had a case where I wanted to use npm in my ubuntu installation but it was giving me this error:
```
npm -v
/usr/bin/env: ‘bash\r’: No such file or directory
```
The solution is to edit `/etc/wsl.conf` to append the following:
[interop]
appendWindowsPath = false  

Then, `wsl --shutdown` to restart the runtime.
You should be able to use npm in the ubuntu container.