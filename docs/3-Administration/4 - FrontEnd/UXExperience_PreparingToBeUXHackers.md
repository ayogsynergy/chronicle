# Preparing to be UX Hackers

The challenge is that we are problem solvers that solve technical puzzles. As such, we see the world differently and potentially prone to technical tunnel vision.  
We need to become `User-Centered`. 

# User Centered
- UX Professionals research who the user is
- What the user needs

> Putting the _user_ at the _center_ of every _decision_ we make.

> Does the decision I'm making benefit the _user_ or...me?  
Don't be selfish. Do what makes sense for the user.

# Common Pitfalls

## Throwing Data on the screen
Doing what's easier for us, and not what's best for the user. The scope of a screen should be focused and not "busy". It can be overwhelming for user to focus on what data they actually need to accomplish.

## Exposing Tech to the User
Exposing users to tech terms, jargon and other data vs. using normal language. This is true for errors or tech processes on the site, for example.

## Forcing the User to think like a DBA
Making uses work to serve the way the database works vs. giving them something intuivite. We can massage the data later, while having the user enter data seamlessly.

## Messy UI
Giving in to laziness or excuses like "I'm not good at UI" leaving the user with a mess.

## Being Inconsistent
Not paying attention to detail for the whole journey.

## Coding for the "Expert User"
Avoiding the hard work of making something easy to use by using the "expert user" excuse. Be able to compromise by allowing the UX to make sense.

## Coding for edge cases
Working hard to write code that solves non-existent problems.  
This can be a time waster, too.

# Deadly Developer Biases
## Bia Blind Spot
The tendency to not compensate for one's own cognitive biases

## Deformation Professionelle
The tendency to look at things according to the conventions of one's profession, forgetting any broader point of view.

## Semmelweis Reflex
The tendency to reject new evidence the contradicts an established paradigm. New data always can come to light to totally up-end assumptions.  
You have to code to accept the new data, not to ignore it.

## Bandwagon Effect
The tendency to do (or believe things) because many other people do (or believe) the same. It is good to play the opposing argument and avoid the groupthink in a team.

## Status Quo Bias
The tendency for people to like things to stay relatively the same. Things should be encouraged to change in order to improve.

## Reactance
The urge to do the opposite of what someone wants you to do out of a need to resist a perceived attempt to constrain your freedom of choice.