# UX Myth Busters

## Counting Clicks
> The user should never have to click more than _n_ times to do something  

- Users are not robots
- They will click as long as 
    - The cognitive friction is low
    - They are getting closer to their goal
- Clicks matter in highly repetitive tasks
    - Why does my user have to do that so much? (can it be automated?)

## Whitespace
> Whitespace is wasted space! We need to show the user as much data as possible!  
A page can look too busy, if it lacks focus then it causes cognitive dissonance.

## Scrolling is Evil
> Everything should fit on one screen. Users hate to scroll!  
- User tests show that vertical scrolling is no big deal
- Horizontal scrolling is not natural to the user

## Small Details Don't Matter
> It's good enough. Let's just ship it!

- Small details can dramatically improve UX
    - Such things can distract the user and take away from the experience
    - Focusing the user's attention on their goal
- Sell the money making button!
    - Get them closer to facilitating business

## More Choices
> The user may want to do this. The user also may want to do that. Power users want everything!  

- Not a clear path through the happy path.
    - The user facecs forks and crossroads
    - Too many choices can raise Cognitive Friction
- Users will often say YES to more choices
    - Show them the happy path to follow
    - They will follow subconciously 

## Users Make Good Choices
> The users know what they need. They'll select the right option.  

- Most decision making is subconcious
- Mulitple choices can paraylyze the thought process (refer to the previous point)
- Low cognitive friction can help influence good choices

## Users Know What They Want
> The customer is king! They will drive the features and requirements.

- Users might request more than they can handle
- Henry Ford predicted that people would ask for faster horses
    - Users sometimes might not know what they want
- Beware of requests made in group settings (Group Think)
- Find out the users goal to determine the need/value.

## Usuability > Beauty
> Users just care if it's usable. They don't care what it looks like.

- Users judge their initial quality and credibility of an app by its appearance.
- Good visual design lowers Emotional Friction which improves the overal UX

## Let's Do It Like...
> Apple, Microsoft, Google, Amazon, etc. They spend billions on R&D!

- Do not blindly accept a UX if it doesn't fit into the nature of your applciation

## UX is a Project Step
> The code is done. Let's "UX" and ship it!

- UX is a continual and iterative process. 
- Shift left, the earlier is better for integrating UX analysis and integration