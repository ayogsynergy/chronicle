# Mount USB on Linux

Recall that everything on linux abstracted to be a file. It gets a file descriptor and the OS does the heavy lifting of device specific implementation details. As such, we wish to leverage this to mount a USB device and transfer files to/from it.

## Find the mount point of the usb device.
```bash
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 931.5G  0 disk
├─sda1   8:1    0   512M  0 part /boot/efi
└─sda2   8:2    0   931G  0 part /
sdb      8:16   0 931.5G  0 disk
├─sdb1   8:17   0 931.5G  0 part
└─sdb9   8:25   0     8M  0 part
sdc      8:32   0 931.5G  0 disk
├─sdc1   8:33   0 931.5G  0 part
└─sdc9   8:41   0     8M  0 part
sdd      8:48   0 931.5G  0 disk
└─sdd1   8:49   0 931.5G  0 part
```

This will give mount points for the devices. 

## Find dev device details
```bash
sudo blkid
...
...
/dev/sdd1: LABEL="BACKUPDRIVEOFINTEREST" UUID="xxxxxxxxxxxxxxxx" TYPE="ntfs" PARTUUID="xxxxxxxx-xx"
```

## Mount the device
Once the respective device is identified, mount the device on a folder.

`mkdir tmpMount`
`sudo mount /dev/sdd1 tmpMount/`

## Umount the Device
When ready you can unmount the device by the dev name or the path onto which it is mounted.  
`sudo umount /dev/sdd1`

Confirm that the device is umounted:  
```bash
cat /proc/mounts | grep sdd
```
Nothing should come back.

# Find mount points for devices
```bash
cat /proc/mounts | grep sdd
/dev/sdd1 /home/userdirs/mount fuseblk rw,relatime,user_id=0,group_id=0,allow_other,blksize=4096 0 0
```

# Conclusion
You can now interact with the device using the FD.