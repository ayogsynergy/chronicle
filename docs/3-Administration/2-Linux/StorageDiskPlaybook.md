# Storage Disk Playbook
A collection of useful things to do on a linux box

# List the largest directories
```bash
du -sh * | sort -nr
```

# Find a collection of files and list the disk usage for each file
```bash
find /targetDir -type f -iname "*owasp*" | xargs -d '\n' du -h
```

# Find a collection of files and list the disk usage for each file and sort by largest
```bash
find /targetDir -type f -iname "*owasp*" | xargs -d '\n' du | sort -nr
```

# List the ten biggest files in a directory (recursive)
```bash
find /targetDir/ -type f | xargs -d '\n' du | sort -nr | head -n10
```

# Find duplicate files
Find the duplicate files and paste it into a text file
```bash
find . -mindepth 1 -type f -printf '%p %f\n' | sort -t ' ' -k 2,2 | uniq -f 1 --all-repeated=s eparate | cut -d' ' -f1 > duplicate.txt
```