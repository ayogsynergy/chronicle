# Find
Basic usage
`find [startingDir] [expression]`

# Locate a file matching a pattern


# Examples
## Remove files that match in a pattern inside of a directory  
`find /tmp -name core -type f -print | xargs /bin/rm -f`  
Find files named `core` in or below the `/tmp` directory and delete them. This will not work for filenames containing newlines, single or double quotes or spaces.

## Find files case insensitive
```bash
find /search-dir -type f -iname "*owasp*"
```