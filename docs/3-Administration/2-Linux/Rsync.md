# Rsync

This is a useful tool to sync files between source and destination.

Because of the new WSL, rsync is available on Windows 10 with the Ubuntu image.

Refer to the man pages for more details.

# Rsync Playbook

## One way mirror back up (dry run)
> I want to backup my files to an additional source so that I can have as redundancy; however, I want to perform a dry as a sanity check to avoid deleting crucial files.  

If the file is originally in `sourceDir` and in `targetDir` from a previous rsync run, the default behavior is to preserve the file on the target destination. Providing this delete flag will delete any file not in source.
```bash
rsync -av -r --info=progress2 --delete --dry-run /sourceDir targetDir/ | grep deleting
```

This will output only the files found for deletion

## One way mirror backup (Remote Host source to Local Host target)

Sync `sourceFolder` and all recursive files to the `d` drive whilst deleting extraneous files from `d` that are not found in `sourceFolder`
```bash
sudo rsync -av --delete --info=progress2 -v rawrdoe@192.168.2.10:/sourceFolder /mnt/d/
```

## Sync Local Host to Remote Host
This is used when you want to push some changes up to a remote host. 

```bash
rsync -a --info=progress2 * user@host:/path/to/where/you/wish/to/save
```
Can use `--compress` over the internet but it can be counter productive over a LAN where bandwidth is not a limiting factor.

## On local FD
`rsync -av /home/user/from-dir /media/backup/to-dir`  
This will sync all source files to be on the destintation, but not delete any extra files on the destintation directory. 

# Flags
This is not an exhaustive list. Refer to the man page for more details.

## -a archive
Archive mode. From the man pages:
`It equals -rlptgoD (no -H, -A, X)`  

It includes  
- -r, --recursive recurse into directories

- -l, --links copy symlinks as symlinks

- -p, --perms preserve permissions

- -t, --times preserve modification times

- -g, --group preserve group

- -o, --owner preserve owner (super-user only)

- -D same as --devices --specials

- --devices preserve device files (super-user only)

- --specials preserve special files

It excludes  
- -H, --hard-links preserve hard links
- -A, --acl preserve ACLs (implies -p)
- -X, --xattrs preserve extended attributes

## --delete Delete files (turned off by default)
Specifiy `--delete` to turn on files or directories not found in the source. This is more formally known as a mirror strategy.

## Show progress
For long transfers, it is useful to see the progress of the transfer. 

`-P` or `--info=progress2` will show statistics based on the whole transfer. 

## Misc flags
`-z` will allow for data compression for low bandwidth connections. This is a tradeoff between cpu power and bandwidth allocation.