# Kernel

I am reading `Linux Kernel Development` by Robert Love to get more insight into linux and how it functions under the hood.

## Chapter 9 Locks
Locks are crucial for being able to support SMP (symmetcial multiprocessing), thus providing a mechanism to share data whilst avoid race conditions.  
Support for SMP allows for processes to run simultaneously on different processors. In addition, the same kernel code could be running a different process (and potentially leveraging the same data). This means we need a way to lock data so that only one context can use it at a time while other interested contexts wait (busy wait or sleep), until ready to consume the respective data.  
### Knowing what data to protect
- what _does_ need locking?
    - anything that could be access across processes (shared resources)

- What _does not_ need protection 
    - local automatic variables
    - dynamically allocated data strcutures 
    - data accessed by the specific process and ot by other processes. NOTE: a process can execute on only one processor at a time  

### Considerations for writing kernel code
- is the data global or shared?
- is the data shared between a process and/ interrupt?
- if a process is preempted during this critical operation, can the newly scheduled process access the same data?

### Conclusion
These points will help to ensure that kernel code is safe from concurrency race conditions. A good rule of thumb is that if other contexts can _see_ the data, lock it.  
As a closing note on this chapter, lock data, not code.