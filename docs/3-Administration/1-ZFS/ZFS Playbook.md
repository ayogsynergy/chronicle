# ZFS playbook
A recipe for ZFS tasks

# Check health of a pool
Get a single line sitrep of the pools on the system.
```
zpool status -x
all pools are healthy
```

Get expanded sitrep of the zfs pools  
```
zpool status
  pool: sample-pool
 state: ONLINE
  scan: scrub repaired 0B in 0h57m with 0 errors on Sun Oct 11 01:21:40 2020
config:

        NAME                STATE     READ WRITE CKSUM
        sample-pool         ONLINE       0     0     0
          mirror-0          ONLINE       0     0     0
            sdb             ONLINE       0     0     0
            sdc             ONLINE       0     0     0

errors: No known data errors
```

## Using GUIDs
Sometimes its helpful to see the drives by GUID (if replacing, resilvering a drive).
```bash
zpool status -g
  pool: sample-pool
 state: ONLINE
  scan: scrub repaired 0B in 1h14m with 0 errors on Sun Dec 13 01:38:24 2020
config:

        NAME                      STATE     READ WRITE CKSUM
        sample-pool                    ONLINE       0     0     0
          277754285026178817      ONLINE       0     0     0
            12345678901234567890  ONLINE       0     0     0
            09876543210987654321  ONLINE       0     0     0

errors: No known data errors
```
Cross reference this with `blkid`  
```bash
sudo blkid
/dev/sdb1: LABEL="sample-pool" UUID="###############" UUID_SUB="12345678901234567890" TYPE="zfs_member" PARTLABEL="zfs-#########" PARTUUID="#################################"
/dev/sdc1: LABEL="sample-pool" UUID="###############" UUID_SUB="09876543210987654321" TYPE="zfs_member" PARTLABEL="zfs-#########" PARTUUID="#################################"
```

This will give you an idea of which drive you can replace in order to resilver correctly. If you're resilvering in a mirrored configuration, you can do a one-for-one swap.

# Cloning and Backing up
This leverages the use of snapshotting. The end result: a new ZFS filesystem hydrated by the snapshot.  

There is no need to unmount a file system to create a snapshot. Consitency of data is assured.

## Create a snapshot dump
Serialize a snapshot and dump it to a file.
`zfs send zpool-name/filesystem-name@snapshot-name > dump-file-name`  

## Receive a ZFS file system 

Create a new file system, `another-filesystem-name`, on pool `another-zpool-name`

```
zfs receive another-zpool-name/another-filesystem-name <>
```  
## Pipe the stream into receive
```
zfs send zpool-name/filesystem-name@snapshot-name | zfs receive another-zpool-name/another-filesystem-name
```

## Pipe the stream via ssh
```
zfs send zpool-name/filesystem-name@snapshot-name | ssh anothermachine zfs receive another-zpool-name/another-filesystem-name
```

# View property on zfs pool
This is useful if you wish to see what the properties on the pool are set.
`zfs get all sample-pool`

# Expand mirror pool size
We wish to upgrade our mirror pool size; we could add the storage drives and attach them to the pool but unfortunately not enough bays are available for this. This operation
is the equivalent to replacing a faulty drive. **Make sure to have a backup available** prior to doing this.  
## Set autoexpand on (if applicable)
We are replacing our existing drives with bigger ones, as such we can tell ZFS to autoexpand the pool one _all_ drives yield a bigger size.
`sudo zpool set autoexpand=on sample-pool` set the zfs pool to auto expand the size of the pool.  

Or, you can create the pool with the autoexpand property enabled.

`zpool create -o autoexpand=on sample-pool c1t13d0` 

1. Replace one of the older drives with the new one.
2. Run zpool status. You will see that the drive is degraded and zfs gives you action items for this.
```bash
user@host:~$ zpool status
  pool: sample-pool
 state: DEGRADED
status: One or more devices could not be used because the label is missing or
        invalid.  Sufficient replicas exist for the pool to continue
        functioning in a degraded state.
action: Replace the device using 'zpool replace'.
   see: http://zfsonlinux.org/msg/ZFS-8000-4J
  scan: scrub repaired 0B in 1h14m with 0 errors on Sun Dec 13 01:38:24 2020
config:

        NAME                      STATE     READ WRITE CKSUM
        sample-pool                    DEGRADED     0     0     0
          mirror-0                DEGRADED     0     0     0
            sdb                   ONLINE       0     0     0
            09876543210987654321  UNAVAIL      0     0     0  was /dev/sdc1
```

ZFS has reported that one of the drives is unavailable, along with the respective GUID that is the missing drive. At this point, you would have replaced it with a drive in the existing bay since there are no additional bays available.

## Prep the replaced drive with GPT
Confirm that the disk is present but has not been formatted yet with `sudo fdisk -l`  
```bash
sudo fdisk -l
Disk /dev/sdc: 3.7 TiB, 4000787030016 bytes, 7814037168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
```

Then, find the disk by ID. Take note of this ID as we will need it later. It should map to the sdX dev id and will most likely not have any partitions assigned to it.
```bash
ls -la /dev/disk/by-id
...
lrwxrwxrwx 1 root root   9 Dec 26 14:51 ata-SERIAL_NUMBER_HERE -> ../../sdc
...
```

## Create GPT Table
We have to label the drive as GPT in order to partition it (ZFS will do this for us as part of the resilvering process).

`sudo parted /dev/disk/by-id/ata-SERIAL_NUMBER_HERE` is the respective command, where your serial number for the drive will be replaced with the sample version of the command.

```bash
user@host:~$ sudo parted /dev/disk/by-id/ata-SERIAL_NUMBER_HERE
[sudo] password for user:
GNU Parted 3.2
Using /dev/sdc
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) mklabel GPT
(parted) q
Information: You may need to update /etc/fstab.
```

Confirm that this change has effected. We should now see the `Disklabel type: gpt`
```bash
sudo fdisk -l 
...
Disk /dev/sdc: 3.7 TiB, 4000787030016 bytes, 7814037168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: XXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXX
...
```

## Tell ZFS to replace the old drive
We need to tell ZFS the old GUID to replace the new one with.  
Provide the path to the new disk.  
Here is an example command  
```bash
user@host:~$ sudo zpool replace sample-pool 09876543210987654321 /dev/disk/by-id/ata-SERIAL_NUMBER_HERE  
```

ZFS will now go through the process of resilvering the newly added drive.
```bash
user@host:~$ sudo zpool status                                                           
  pool: sample-pool                                                                              
 state: DEGRADED                                                                            
status: One or more devices is currently being resilvered.  The pool will                   
        continue to function, possibly in a degraded state.                                 
action: Wait for the resilver to complete.                                                  
  scan: resilver in progress since Sat Dec 26 15:43:22 2020                                 
        243M scanned out of 459G at 27.0M/s, 4h50m to go                                    
        239M resilvered, 0.05% done                                                         
config:                                                                                     
                                                                                            
        NAME                                            STATE     READ WRITE CKSUM          
        sample-pool                                     DEGRADED     0     0     0          
          mirror-0                                      DEGRADED     0     0     0          
            sdb                                         ONLINE       0     0     0          
            replacing-1                                 DEGRADED     0     0     0          
              09876543210987654321                      UNAVAIL      0     0     0  was /dev sdc1                               
              ata-SERIAL_NUMBER_HERE                    ONLINE       0     0     0  (resilvering)                                                                                 
                                                                                            
errors: No known data errors                                                                
```

Give it time to finish resilvering and proceed to the remaining drive(s) as part of completing all members of the mirror.


## Second drive replacement
The outputs after replacing the second larger capacity drive. Notice the EXPANDSZ 
```bash
zpool list
NAME     SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
sample-pool   928G   460G   468G     2.73T     0%    49%  1.00x  DEGRADED  -
```