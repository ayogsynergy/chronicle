# ZFS Command Reference

# Pool information  
|Command            |Descriptions           |
|-------            |------------           |
|zpool status -x    |show pool status      |
|zpool list         |show all the pools