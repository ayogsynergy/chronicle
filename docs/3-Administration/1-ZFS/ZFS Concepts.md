# ZFS Concepts

# Concepts
## snapshot
a read only version of a file system or volume at a given point in time. It is specified by filesystem@name or volume@name

### List snapshots
```
zfs list -t snapshot
```

## Dataset

## Set Quota on dataset  
Set a cap on the amount of data into the folder `/poolname/datasetname`  
```
zfs set quota-100G poolname/datasetname
```

# Compression
ZFS inline compression is off by default.  
`LZ4` offers rapid compression and decompression. It is great for all general use cases. The performance pentalty for incompressable data (jpg, png, etc) is small; while the performance gain is significant.

`compression=lz4`  
