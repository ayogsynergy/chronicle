# Rx Java

Observer pattern process

Notified when:
- data changes
- task completes
- errors happen

# Java Observables (avoid this)
- complex
- little to no examples
- use with layered architecture
- not very kotlinish

# Rx Java Continued
- has common patterns for events, data changes
- filter, map values to new results
- chain actions

# Observables
they are at the core of rxjava
observables are a list of values that you can iterate over (a stream)

they are things that you can watch (as an oberserver, aka a subscriber)

# Types of observables
- unbounded lists of events (taps or button clicks)
- single variable that notifies the changes over time
- tasks of code that notifies when its finished
- bounded array of elements

# Observable examples
- new event: unbounded list of events (UI Events)
- change event: single variable (a title change after a data update)
- complete event: tasks of code (network tasks)
- next element event: processing a list of user ids

# Observer code

```kotlin
val observable = Observable.fromArray(1,2,3,4,5,6,7,8,9)

// recall that nothing happens up to this point until a subscriber subscribes

val disposable = observable.subscribe({ number: Int -> // onNext
    println(number)
}, { error -> // onError
    println("some error happened: $error")
}, { // onCompleted
    println("No more elements... ever")
}, { // onSubscribe
    println("subscription is completed")
})
disposable.disposedBy(bag)
```

prepare to clean up your disposable when you're setting them up

# Relays
Relays are good because they don't have an onError or onComplete, they just keep on chuggin along. As such, they are a good candidate to tie into the UI where we may not want to fail and keep showing the latest data available prior to failure.  
It depends on the use case, maybe we do want to show something went wrong, then the relay would not be a good candidate in that case.

# Real life use cases
Observables we want to consume generally but they are useful for:
- network / db / file and i/o work
- long running task
- when ever we want to know a task finishes


# Architecture Guidelines
this comes into question when managing how RxJava will interface with different layers.

Tip: design all apis as asynchronous methods instead of returning a value directly.

Recall that Asynchronous API keeps the model layer from blocking the UI. Even if the DB layer returns immediately, it would be more standard if all consumers are using the same type of API (asynchronous). This way, they can just assume that the callback will be used when the data is ready.

The second guideline is to return observables when ever possible.

the soon values can be put into observerables, this helps keep class less concerned about how something is implemented (delegates, interfaces) when maintaining subscriptions. 
instead they react to the _state_ of things. 

they focus more on their own _internal_ state and not triggering actions on outside things to get what they need (leaky abstractions)

- always make sure you're observing on the main thread. to be safe, if you observe on the main thread right before you subscribe, you won't have any problems. this means the subscribe method will run on the main thread. generally you would do this principle for things related to the view layer. if you need to do other work then you can do this else where.

