# Lynda site
https://www.lynda.com/MyPlaylist/Watch/22131729/691628?autoplay=true

# Async and Reactive Basics

Async means not occurring at the same time.

With angular and two-way data binding, dynamic content was possible. Data could be displayed even if it wasn't there yet.

This type of binding (synchronous) isn't a good fit. We can solve this with a callback. The code is smart enough to wait for some code to complete before continuing.

Recall the callback from hell example.  
Also can't return values from a callback.

Promises were nice for error handling, could chain and could also return values.  
Promises exposes a then to indicate when it is finished.

however, we want messages delivered instantly and in real time. Promises can achieve this but promises can only be used once.

We want a stream of events.

An observable is a reusable promise than can be re-used as a .then method. This is done via a subscribe method.

Every time the observer fires the .then, the subscribe will pick that event up.

# Gang of four
design ideas published in 1994. 

Christopher Alexander said that pattern design describes a problem and solution to the that problem.

# Observer Pattern
## Definition from google
Software design pattern in which an object (called a subject) maintains a list of its dependents (subscribers or observers)
and notifies them automatically of any state changes usually by calling one of their state change methods.

Basically it works well for an application needing real time SLAs. This is also tied to event driven design.

The observer pattern is concerned around events and real time updates.  

# Database Observables
Real time data has become standard and expected.  
DB usually store data and wait for a query.
Now, we want DB to notify us when data has changed.
We change how to interact with the DB. We can listen for updates.

Before, polling was the only option.

Data stream: listening to dynamic data.
A stream can be thought of items on a conveyor belt in little pieces worked on bit by bit.

Marble diagrams are synonymous as to streams.

# Common mistakes
Operators are used to modify observables without unwrapping them.
Interval will increase the value by one for each period you wait.

# take 
`interval` specifies how long to wait before incrementing and emitting a value
`take` specifies how many events to allow to be piped before it is stopped.
```javascript
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/take';

// take first five values of this observable and then stop.
const numbers$ = Observable.interval(1000).take(5);
numbers$.subscribe(x => console.log(x));
```

# map
The code below will take 5 events and transform it (using map) before trying to send it off to the subscribe method.
```javascript
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/take';
import 'rxjs/add/observable/map';

const numbers$ = Observable.interval(1000);

numbers$
  .take(5)
  .map(x => {
    return x * 10;
  })
  .subscribe(x => console.log(x));
```

# filter
filter can decide when it can pipe the 
```javascript
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/take';
import 'rxjs/add/observable/map';
import 'rxjs/add/observable/filter';

const numbers$ = Observable.interval(1000);

numbers$
  .take(5)
  .map(x => {
    return x * 10;
  })
  .filter(x => x > 20)
  .subscribe(x => console.log(x));
```

Remember that the order of these operators matters

# of and mergeMap

Can combine different streams with mergeMap.
of creates an observable out of an array or even a promise
```javascript
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/take';
import 'rxjs/add/observable/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/filter';
import 'rxjs/add/observable/mergeMap';

const numbers$ = Observable.interval(1000);
const letters$ = Observable.of('a', 'b', 'c', 'd', 'e');
letters$.mergeMap(x => {
  numbers$
    .take(5)
    .map(i => i + x)    
}).subscribe(x => console.log(x));


numbers$
  .take(5)
  .map(x => {
    return x * 10;
  })
  .filter(x => x > 20)
  .subscribe(x => console.log(x));
```

# switchMap
imagine a search bar than queries a DB
It will cancel an old request and replace it with a new one automatically (if waiting for a query to return while the user updates their search term)
switchMap will trigger the subscribe once for each number but it will only give us the most recent letter each time it fires rather than all the letters.

```javascript
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/take';
import 'rxjs/add/observable/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/filter';
// this is the difference
import 'rxjs/add/observable/switchMap';

const numbers$ = Observable.interval(1000);
const letters$ = Observable.of('a', 'b', 'c', 'd', 'e');
letters$.switchMap(x => {
  numbers$
    .take(5)
    .map(i => i + x)    
}).subscribe(x => console.log(x));


numbers$
  .take(5)
  .map(x => {
    return x * 10;
  })
  .filter(x => x > 20)
  .subscribe(x => console.log(x));
```

# Creating an observable
This is a basic observer.

An observer has next, error, and complete.
It could be a button, search box, remote data stream, anything.
The observer is the inner trigger (of an observable) for when something changes
The job of the observer is to carry that trigger to all of the subscribers

```javascript
import { Observable } from 'rxjs/Observable';

const observable$ = Observable.create(ob => {
  ob.next(1);
  ob.next(2);
  ob.next(3);
  ob.complete();
});

observable$.subscribe(
  value => console.log(value),
  err => {}, // do nothing bro
  () => console.log('this is the end') // on complete
);
```

# Pitfall
The observable will still be listening even when navigating off of a page.
Angular provides a lifecycle hook to clean up the subscriber when the page leaves a page.
`ngOnDestroy`

# Subject
Recall that observables are the outer wrapper that allow you to listen to an observer. Each time you create an observable, you get a single instance.
You cannot reuse observables and the observers cannot interact with each other.

Each subscription will not know about each other. You can subscribe as much as you want.

To address this design implementation: a change was suggested to use a Subject. 
A subject is both an observer and an observable.
The subject exposes the .next .subscribe from outside. This is as opposed to only having access to the observer in the initialization of it.

```javascript
import 'rxjs/Subject';

const mySubject$ = new Subject();
mySubject$.subscribe(x => console.log('first subscribe', x));
mySubject$.next(1);
mySubject$.next(2);

mySubject$.subscribe(x => console.log('second subscribe', x));
mySubject$.next(3);
// unsub
mySubject$.unsubscribe();
```

In the code above, the first subscriber gets all the values but the second subscriber gets the values after the subscription has been registered. 

Subjects are shareable but they are not reusable. Once errors and complete has been triggered, it will ignore any following subscriptions.

`subject$.complete()` is how to denote a completion. Use `.unsubscribe()` to at least know when a subscription has happened afterward to fix any race conditions.

# Behavior Subject
 Its just like a subject but it requires a value to initialize and it only holds the latest recent value for any new subscribers.
 
 This is similar to what i was using in the intelectric app
 
 # Replay Subject
 The replay subject does not require any initial value.
 
 The events are recorded and can be "replayed" for new subscribers that register later on down the road.
 
 # async pipe in angular
 allows her to use values in the observable but I don't think this applies in React.  
 Basically what she is trying to get across is that there are operators available for observables and you can leverage them to achieve what you're looking for.