# Rx Resources
# Visualization
https://rxmarbles.com/

# Marble Diagram
This is the [reactivx.io](https://reactivex.io)  
Recall the marble diagram for debounce.

# Notes
recall that operators like map, flatMap, reuse and filter are available.
The operators transform the values emitted by observables.
An operator doesn't have to be unary, it can combine multiple observables to provide syncronization between them.