# Chronicle

A collection of technology focused concepts that interest me.


# About
I am Gerardo Sanchez, I am passionate about Software Development with an emphasis in Cyber Security.
Find me on [Linkedin](https://www.linkedin.com/in/gerardo-sanchez-36470393/).

# Certifications

## CompTIA Security +
![security plus certification](assets/cert/secplus_ce.png)